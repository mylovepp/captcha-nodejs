const captcha = require('../controllers/captcha.controller');

module.exports = function (app) {

    app.get('/captcha', captcha.createCaptcha);

    app.post('/captcha', captcha.validate);
    
};