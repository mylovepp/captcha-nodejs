const svgCaptcha = require('svg-captcha');
const uuidv4 = require('uuid/v4');

//Should be database
var captchaStorage = [];

exports.createCaptcha = function (req, res, next) {
    const options = {
        ignoreChars: '0oO1lI',
        size: 6,
        noise: 3,
        color: false,
        width: 150,
        height: 100,
        fontSize: 50,
    };


    const captchaId = uuidv4();

    const captcha = svgCaptcha.create(options);
    const captchaSession = {
        captchaId: captchaId,
        captchaText: captcha.text
    }
    const captchaToken = {
        captchaId,
        captcha: captcha.data
    }

    captchaStorage.push(captchaSession);

    res.status(200).send(captchaToken);
};

exports.validate = function (req, res, next) {
    const captchaId = req.body ? req.body.captchaId : undefined;
    const captchaText = req.body ? req.body.captchaText : undefined;

    const expectedCaptchaIndex = captchaStorage && Array.isArray(captchaStorage) &&
        captchaStorage.findIndex(item => item.captchaId === captchaId &&
            item.captchaText === captchaText);

    if (expectedCaptchaIndex > -1) {

        //remove from session when already used
        captchaStorage.splice(expectedCaptchaIndex, 1);

        res.status(200).send({
            statusCode: '0000',
            message: 'Captcha Correct'
        });
    } else {
        res.status(200).send({
            statusCode: '9000',
            message: 'Invalid Captcha'
        });
    }
};