var config = require('./config');
var express = require('express');
var morgan = require('morgan');
var compression = require('compression');
var bodyPraser = require('body-parser');
var session = require('express-session');

module.exports = function () {
    var app = express();


    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    } else {
        app.use(compression());
    }

    //=====Save Session in server's' memory
    app.use(session({
        genid: (req) => config.sessionSecret,
        secret: config.sessionSecret,
        resave: true,
    }));


    app.use(bodyPraser.urlencoded({
        extended: true
    }));

    app.use(bodyPraser.json());

    require('../app/routes/global.route')(app);
    require('../app/routes/captcha.route')(app);

    return app;

};