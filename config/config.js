process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.PORT = process.env.PORT || 30001;

module.exports = require('./env/' + process.env.NODE_ENV + '.js');