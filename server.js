var express = require('./config/express');

var app = express();

var server = app.listen(process.env.PORT, function () {
    
    
    console.log('Server is running at http://localhost:' + process.env.PORT);
});



app.get('/', function (req, res) {
    res.send('User Service is ALIVE!')
});
app.get('/info', function (req, res) {
    res.send({})
})

module.exports = app;